package db

import (
	"log"
	"os"

	"github.com/robfig/cron"
)

func filePrint() {

	var myLogger *log.Logger

	fpLog, err := os.OpenFile("logfile.txt", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	defer fpLog.Close()

	myLogger = log.New(fpLog, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)

	myLogger.Print("Test")

	myLogger.Println("End of Program")
}

func Scheduler() {
	c := cron.New()

	_ = c.AddFunc("* * * * * *", func() {
		filePrint()
	})

	c.Start()
}
